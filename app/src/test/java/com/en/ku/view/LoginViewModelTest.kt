package com.en.ku.view

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.test.espresso.matcher.ViewMatchers.assertThat
import com.en.ku.base.BaseUTTest
import com.en.ku.data.rest.repository.GeneralRepository
import com.en.ku.di.module.networkModule
import com.en.ku.di.module.repositoryModule
import com.en.ku.di.module.utilityModule
import com.en.ku.di.module.viewModelModule
import com.en.ku.vo.model.body.BodyLogin
import com.en.ku.vo.model.response.ResponseLogin
import io.reactivex.observers.TestObserver
import kotlinx.coroutines.runBlocking
import org.hamcrest.CoreMatchers.notNullValue
import org.hamcrest.Matchers.`is`
import org.junit.*
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.koin.core.context.startKoin
import org.koin.core.context.stopKoin
import org.koin.test.inject


@RunWith(JUnit4::class)
class LoginViewModelTest: BaseUTTest() {

    private val generalRepository: GeneralRepository  by inject()

    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    private var userName = "gobank@gmail.com"
    private var password = "password"
    private var token = "7C57196B27D826A2165F382821CF37C57196B27D826A2165F382821CF3"
    private var language = "th"

    @Before
    fun start(){
        super.setUp()
        startKoin {
            modules(arrayListOf(networkModule, utilityModule, repositoryModule, viewModelModule))
        }
    }

    @Test
    fun testLoginSuccess_returnResponse() =  runBlocking {
        //mockNetworkResponseWithFileContent("objectLogin.json", HttpURLConnection.HTTP_OK)

        val dataReceived = generalRepository.onLogin(BodyLogin(userName, password,token,language))

        val testObserver = TestObserver<ResponseLogin>()
        dataReceived.subscribe(testObserver)

        assertThat(testObserver.values()[0].data, `is`(notNullValue()))
    }

    @Test
    fun testLoginCheckName_returnResponseFullName() =  runBlocking {
        //mockNetworkResponseWithFileContent("objectLogin.json", HttpURLConnection.HTTP_OK)

        val dataReceived = generalRepository.onLogin(BodyLogin(userName,
            password,token,language))
        val testObserver = TestObserver<ResponseLogin>()
        dataReceived.subscribe(testObserver)

        assertThat(testObserver.values()[0].data.fullname, `is`("Vittavach"))
    }

    @Test
    fun testLoginError_returnNoResponse() =  runBlocking {
        //mockNetworkResponseWithFileContent("objectLogin.json", HttpURLConnection.HTTP_OK)

        val dataReceived = generalRepository.onLogin(BodyLogin("gobank2@gmail.com",
            "password","","th"))
        val testObserver = TestObserver<ResponseLogin>()
        dataReceived.subscribe(testObserver)

        assertThat(testObserver.values().isEmpty(), `is`(true))
    }

    @After
    override fun tearDown() {
        stopKoin()
    }

}