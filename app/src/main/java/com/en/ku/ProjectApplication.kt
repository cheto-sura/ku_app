package com.en.ku

import android.app.Application
import androidx.multidex.MultiDex
import com.en.ku.di.module.networkModule
import com.en.ku.di.module.repositoryModule
import com.en.ku.di.module.utilityModule
import com.en.ku.di.module.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin


class ProjectApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        MultiDex.install(this)

        startKoin {
            androidContext(this@ProjectApplication)
            modules(arrayListOf(networkModule, utilityModule, repositoryModule, viewModelModule))
            androidLogger()
        }
    }
}
