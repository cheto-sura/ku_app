package com.en.ku.utils.facebook

enum class FacebookGetType {
    LOGIN,
    REGISTER
}
