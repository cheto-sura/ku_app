package com.en.ku.utils.rxBus

object EventRxBus {

    fun <RequestType> onAddEventRxBus(rxClass: RequestType) {
        RxBus.publish(rxClass)
    }
}
