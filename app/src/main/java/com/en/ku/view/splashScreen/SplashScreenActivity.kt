package com.en.ku.view.splashScreen

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import androidx.databinding.DataBindingUtil
import com.en.ku.R
import com.en.ku.data.Constants
import com.en.ku.databinding.ActivitySplashScreenBinding
import com.en.ku.view.base.BaseActivity
import com.en.ku.view.home.HomeActivity
import com.en.ku.view.login.PreLoginActivity
import qiu.niorgai.StatusBarCompat

class SplashScreenActivity : BaseActivity() {

    private lateinit var binding: ActivitySplashScreenBinding

    private var mHandler = Handler()

    var newsId = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        initView()
        initViewModel()
    }

    private fun initView() {
        StatusBarCompat.translucentStatusBar(this, true)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_splash_screen)
        newsId = intent.getStringExtra("news_id")?:""
    }

    private fun initViewModel() {
        onCheckTokenIntentMain()
    }

    private fun onCheckTokenIntentMain() {
        startApp(if (mPreferences.getToken().isNotEmpty()) "intentMain" else "login")
    }

    private fun startApp(statusIntent: String) {
        var intent = Intent()
        when (statusIntent) {
            "login" -> {
                intent = Intent(this@SplashScreenActivity, PreLoginActivity::class.java)
            }
            "intentMain" -> {
                intent = Intent(this, HomeActivity::class.java).putExtra("news_id",newsId)
            }
        }
        mHandler.postDelayed({
            startActivity(intent)
            finishAffinity()
        }, Constants.mTimeLoadPage)
    }
}
