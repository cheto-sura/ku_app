package com.en.ku.view.home

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.en.ku.R
import com.en.ku.databinding.ItemMenuBinding
import com.en.ku.utils.SingleLiveData
import com.en.ku.vo.model.response.ModelMenu
import java.util.*

class AdapterMenuList(
    private var mContext: Context,
    private var mListMenu: ArrayList<ModelMenu>,
    private var mOnClickList: SingleLiveData<Int>
) : RecyclerView.Adapter<AdapterMenuList.ViewHolder>() {

    override fun getItemCount(): Int {
        return mListMenu.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ViewHolder {
        val binding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context), R.layout.item_menu, parent, false
        ) as ItemMenuBinding

        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        bindText(holder, position)

        holder.binding.root.setOnClickListener {
            mOnClickList.value = mListMenu[position].id
        }
    }

    private fun bindText(holder: ViewHolder, position: Int) {
        holder.binding.ivPhoto.setImageResource(mListMenu[position].photo)
        holder.binding.tvName.text = mListMenu[position].name
    }

    class ViewHolder(internal var binding: ItemMenuBinding) :
        RecyclerView.ViewHolder(binding.root)
}
