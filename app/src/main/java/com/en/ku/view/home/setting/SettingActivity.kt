package com.en.ku.view.home.setting

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.en.ku.R
import com.en.ku.databinding.ActivitySettingBinding
import com.en.ku.view.base.BaseActivity
import com.en.ku.view.login.PreLoginActivity
import org.koin.androidx.viewmodel.ext.android.viewModel
import qiu.niorgai.StatusBarCompat

class SettingActivity : BaseActivity() {

    private val viewModel: SettingViewModel by viewModel()

    private lateinit var binding: ActivitySettingBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        initView()
        initViewModel()
    }

    private fun initView() {
        StatusBarCompat.translucentStatusBar(this, true)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_setting)
        toolbarViewModel.titleToolbarView.set(resources.getString(R.string.menu_setting))
    }

    private fun initViewModel() {
        binding.dataViewModel = viewModel
        binding.toolbarViewModel = toolbarViewModel

        onSubscriptViewModel()
        onSubscriptOnClick()
    }

    private fun onSubscriptViewModel() {

    }

    private fun onSubscriptOnClick() {
        viewModel.mOnClickListener.observe(this, Observer {
            when(it){
                "profile" -> {
                    Toast.makeText(this,it,Toast.LENGTH_SHORT).show()
                }
                "language" -> {
                    Toast.makeText(this,it,Toast.LENGTH_SHORT).show()
                }
                "logout" -> {
                    mPreferences.clearDataLogout()
                    val intentApp = Intent(this, PreLoginActivity::class.java)
                    startActivity(intentApp)
                    finishAffinity()
                    startIntentAnimation(false)
                }
            }
        })

        toolbarViewModel.onClickToolbar.observe(this, Observer {
            when (it) {
                "intentBack" -> {
                    this.onBackPressed()
                }
                else -> {
                    print("no event")
                }
            }
        })
    }

    override fun onBackPressed() {
        finish()
        startIntentAnimation( false)
    }

}
