package com.en.ku.view.home

import androidx.lifecycle.ViewModel
import com.en.ku.domain.GeneralUseCase
import com.en.ku.utils.SingleLiveData

class HomeViewModel (private val generalUseCase: GeneralUseCase) : ViewModel() {

    val onClickItemList = SingleLiveData<Int>()

}
