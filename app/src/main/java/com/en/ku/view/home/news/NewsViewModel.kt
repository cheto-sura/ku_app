package com.en.ku.view.home.news

import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.en.ku.domain.GeneralUseCase
import com.en.ku.utils.SingleLiveData
import com.en.ku.vo.Resource
import com.en.ku.vo.model.response.ModelNews
import com.en.ku.vo.model.response.ResponseNews
import com.en.ku.vo.model.response.ResponseRefreshToken
import com.en.ku.vo.model.response.ResponseUser

class NewsViewModel (private val generalUseCase: GeneralUseCase) : ViewModel() {

    val onClickItemList = SingleLiveData<ResponseNews.Item>()

    val isLoadDuplicate = ObservableField(true)

    val mPage = ObservableField(1)

    val mNewsTypeId = ObservableField(3)

    var mNewsCall = SingleLiveData<Void>()
    val mResponseNews: LiveData<Resource<ResponseNews>> = Transformations
            .switchMap(mNewsCall) {
                generalUseCase.onNews(mNewsTypeId.get()!!,mPage.get()!!)
            }

    var mRefreshTokenCall = SingleLiveData<Void>()
    val mResponseRefreshToken: LiveData<Resource<ResponseRefreshToken>> = Transformations
            .switchMap(mRefreshTokenCall) {
                generalUseCase.onRefreshToken()
            }

}
