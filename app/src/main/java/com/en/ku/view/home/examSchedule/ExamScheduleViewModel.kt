package com.en.ku.view.home.examSchedule

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.en.ku.domain.GeneralUseCase
import com.en.ku.utils.SingleLiveData
import com.en.ku.vo.Resource
import com.en.ku.vo.model.response.ModelNews
import com.en.ku.vo.model.response.ResponseExamSchedule
import com.en.ku.vo.model.response.ResponseRefreshToken

class ExamScheduleViewModel (private val generalUseCase: GeneralUseCase) : ViewModel() {

    val onClickItemList = SingleLiveData<ModelNews>()

    var mExamScheduleCall = SingleLiveData<Int>()
    val mResponseExamSchedule: LiveData<Resource<ResponseExamSchedule>> = Transformations
            .switchMap(mExamScheduleCall) {
                generalUseCase.onExamSchedule()
            }

    var mRefreshTokenCall = SingleLiveData<Void>()
    val mResponseRefreshToken: LiveData<Resource<ResponseRefreshToken>> = Transformations
            .switchMap(mRefreshTokenCall) {
                generalUseCase.onRefreshToken()
            }

}
