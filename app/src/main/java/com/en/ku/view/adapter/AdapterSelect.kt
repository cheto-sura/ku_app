package com.en.ku.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.en.ku.R
import com.en.ku.databinding.ItemSelectListBinding
import java.util.*

class AdapterSelect(
    private var mListProduct: ArrayList<String>,
    private var mOnClickList: (Int,String)->Unit
) : RecyclerView.Adapter<AdapterSelect.ViewHolder>() {

    override fun getItemCount(): Int {
        return mListProduct.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ViewHolder {
        val binding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context), R.layout.item_select_list, parent, false
        ) as ItemSelectListBinding

        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        bindText(holder, position)
        holder.binding.root.setOnClickListener {
            mOnClickList.invoke(position,mListProduct[position])
        }
    }

    private fun bindText(holder: ViewHolder, position: Int) {
        holder.binding.tvTitle.text = mListProduct[position]
        if (position == mListProduct.size-1)
            holder.binding.line.visibility = View.GONE
        else
            holder.binding.line.visibility = View.VISIBLE
    }

    class ViewHolder(internal var binding: ItemSelectListBinding) :
        RecyclerView.ViewHolder(binding.root)
}
