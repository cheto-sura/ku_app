package com.en.ku.view.home.notification

import android.content.Intent
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.bumptech.glide.Glide
import com.en.ku.R
import com.en.ku.databinding.ActivityNotificationDetailBinding
import com.en.ku.view.base.BaseActivity
import com.en.ku.view.home.notification.activityFrom.ActivityFromActivity
import com.en.ku.view.login.PreLoginActivity
import com.en.ku.vo.enumClass.Status
import org.koin.androidx.viewmodel.ext.android.viewModel
import qiu.niorgai.StatusBarCompat

class NotificationDetailActivity : BaseActivity() {

    private val viewModel: NotificationDetailViewModel by viewModel()

    private lateinit var binding: ActivityNotificationDetailBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        initView()
        initViewModel()
    }

    private fun initView() {
        StatusBarCompat.translucentStatusBar(this, true)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_notification_detail)
    }

    private fun initViewModel() {
        binding.dataViewModel = viewModel
        binding.toolbarViewModel = toolbarViewModel

        onSubscriptViewModel()
        onSubscriptOnClick()
    }

    override fun onResume() {
        super.onResume()
        viewModel.mNewsCall.value = intent.getIntExtra("id",0)
    }

    private fun onSubscriptViewModel() {
        viewModel.mResponseNews.observe(this, Observer {
            binding.loadResource = it
            when (it.status) {
                Status.SUCCESS -> {
                    toolbarViewModel.titleToolbarView.set(it.data!!.topic)
                    viewModel.mDetail.set(it.data.detail)
                    Glide.with(this).load(it.data.thumbnail).into(binding.ivPhoto)
                }
                Status.ERROR -> {
                    if (it.message.equals("401")){
                        viewModel.mRefreshTokenCall.call()
                    }else{
                        mDialogPresenter.dialogMessage(
                                resources.getString(R.string.message_alert_dialog),
                                it.message
                        ) {}
                    }
                }
                Status.LOADING -> {
                }
            }
        })

        viewModel.mResponseRefreshToken.observe(this, Observer {
            binding.loadResource = it
            when (it.status) {
                Status.SUCCESS -> {
                    mPreferences.saveToken(it.data!!.accessToken?:"")
                    viewModel.mNewsCall.value = viewModel.mNewsCall.value
                }
                Status.ERROR -> {
                    if (it.message.equals("401")){
                        val intentApp = Intent(this, PreLoginActivity::class.java)
                        startActivity(intentApp)
                        finishAffinity()
                        startIntentAnimation(false)
                    }else{
                        mDialogPresenter.dialogMessage(
                                resources.getString(R.string.message_alert_dialog),
                                it.message
                        ) {}
                    }
                }
                Status.LOADING -> {
                }
            }
        })
    }

    private fun onSubscriptOnClick() {
        viewModel.mOnClickListener.observe(this, Observer {
            val intentApp = Intent(this, ActivityFromActivity::class.java)
            startActivity(intentApp)
            startIntentAnimation(true)
        })

        toolbarViewModel.onClickToolbar.observe(this, Observer {
            when (it) {
                "intentBack" -> {
                    this.onBackPressed()
                }
                else -> {
                    print("no event")
                }
            }
        })
    }

    override fun onBackPressed() {
        finish()
        startIntentAnimation( false)
    }
}
