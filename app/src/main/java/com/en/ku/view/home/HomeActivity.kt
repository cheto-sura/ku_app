package com.en.ku.view.home

import android.content.Intent
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import com.en.ku.R
import com.en.ku.databinding.ActivityHomeBinding
import com.en.ku.view.base.BaseActivity
import com.en.ku.view.home.examSchedule.ExamScheduleActivity
import com.en.ku.view.home.news.NewsActivity
import com.en.ku.view.home.news.NewsDetailActivity
import com.en.ku.view.home.newsEngineer.NewsEngineerActivity
import com.en.ku.view.home.notification.NotificationActivity
import com.en.ku.view.home.profile.ProfileActivity
import com.en.ku.view.home.setting.SettingActivity
import com.en.ku.view.login.PreLoginActivity
import com.en.ku.vo.model.response.ModelMenu
import org.koin.androidx.viewmodel.ext.android.viewModel
import qiu.niorgai.StatusBarCompat

class HomeActivity : BaseActivity() {

    private val viewModel: HomeViewModel by viewModel()

    private lateinit var binding: ActivityHomeBinding

    private var mListDataMenuList = ArrayList<ModelMenu>()

    private val mCustomAdapterOrderList by lazy {
        AdapterMenuList(this, mListDataMenuList, viewModel.onClickItemList)
    }

    var mUserType = "student"
    var newsId = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        initView()
        initViewModel()
    }

    private fun initView() {
        StatusBarCompat.translucentStatusBar(this, true)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_home)
    }

    private fun initViewModel() {
        binding.dataViewModel = viewModel

        getIntentData()
        initMenuList()
        onSubscriptViewModel()
        onSubscriptOnClick()
        if (newsId.isNotEmpty()){
            val intentApp = Intent(this, NewsDetailActivity::class.java)
            intentApp.putExtra("id",newsId.toInt())
            startActivity(intentApp)
            startIntentAnimation(true)
        }
    }

    private fun getIntentData(){
        mUserType = intent.getStringExtra("userType")?:"student"
        newsId = intent.getStringExtra("news_id")?:""
    }

    private fun initMenuList() {
        when (mUserType){
            "student" -> {
                mListDataMenuList.add(ModelMenu(0,getString(R.string.menu_notification),R.drawable.megaphone))
                mListDataMenuList.add(ModelMenu(1,getString(R.string.menu_news_of_engineer),R.drawable.newspaper))
                mListDataMenuList.add(ModelMenu(2,getString(R.string.menu_news),R.drawable.calendar))
                mListDataMenuList.add(ModelMenu(3,getString(R.string.menu_student),R.drawable.student))
                mListDataMenuList.add(ModelMenu(4,getString(R.string.message_exam_schedule),R.drawable.test))
                mListDataMenuList.add(ModelMenu(5,getString(R.string.message_logout),R.drawable.logout))
            }
            "parent" -> {
                mListDataMenuList.add(ModelMenu(1,getString(R.string.menu_news_of_engineer),R.drawable.newspaper))
                mListDataMenuList.add(ModelMenu(3,getString(R.string.menu_student),R.drawable.student))
                mListDataMenuList.add(ModelMenu(5,getString(R.string.message_logout),R.drawable.gears))
            }
        }

        binding.recyclerViewMenu.apply {
            layoutManager = GridLayoutManager(this@HomeActivity,3)
            adapter = mCustomAdapterOrderList
        }
        mCustomAdapterOrderList.notifyDataSetChanged()
    }

    private fun onSubscriptViewModel() {

    }

    private fun onSubscriptOnClick() {
        viewModel.onClickItemList.observe(this, Observer {
            when(it){
                0 -> {//แจ้งเตือน
                    val intentApp = Intent(this, NotificationActivity::class.java)
                    startActivity(intentApp)
                    startIntentAnimation(true)
                }
                1 -> {//ข่าวคณะวิศวกรรมศาสตร์
                    val intentApp = Intent(this, NewsEngineerActivity::class.java)
                    startActivity(intentApp)
                    startIntentAnimation(true)
                }
                2 -> {//ข่าวกิจกรรม
                    val intentApp = Intent(this, NewsActivity::class.java)
                    startActivity(intentApp)
                    startIntentAnimation(true)
                }
                3 -> {//ข้อมูลนิสิต
                    val intentApp = Intent(this, ProfileActivity::class.java)
                    startActivity(intentApp)
                    startIntentAnimation(true)
                }
                4 -> {//ตารางสอบ
                    val intentApp = Intent(this, ExamScheduleActivity::class.java)
                    startActivity(intentApp)
                    startIntentAnimation(true)
                }
                5 -> {//ออกจากระบบ
                    mDialogPresenter.dialogMessageTwoButton(getString(R.string.app_name),"ต้องการออกจากระบบใช่หรือไม่"){
                        if (it){
                            mPreferences.clearDataLogout()
                            val intentApp = Intent(this, PreLoginActivity::class.java)
                            startActivity(intentApp)
                            finishAffinity()
                            startIntentAnimation(false)
                        }
                    }
                }
            }
        })
    }
}
