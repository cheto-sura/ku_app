package com.en.ku.view.home.news

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.en.ku.R
import com.en.ku.databinding.ActivityNewsDetailBinding
import com.en.ku.view.base.BaseActivity
import com.en.ku.view.home.notification.activityFrom.ActivityFromActivity
import com.en.ku.view.login.PreLoginActivity
import com.en.ku.vo.enumClass.Status
import org.koin.androidx.viewmodel.ext.android.viewModel
import qiu.niorgai.StatusBarCompat

class NewsDetailActivity : BaseActivity() {

    private val viewModel: NewsDetailViewModel by viewModel()

    private lateinit var binding: ActivityNewsDetailBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        initView()
        initViewModel()
    }

    private fun initView() {
        StatusBarCompat.translucentStatusBar(this, true)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_news_detail)
    }

    private fun initViewModel() {
        binding.dataViewModel = viewModel
        binding.toolbarViewModel = toolbarViewModel

        onSubscriptViewModel()
        onSubscriptOnClick()
    }

    override fun onResume() {
        super.onResume()
        viewModel.mNewsCall.value = intent.getIntExtra("id",0)
    }

    private fun onSubscriptViewModel() {
        viewModel.mResponseNews.observe(this, Observer {
            binding.loadResource = it
            when (it.status) {
                Status.SUCCESS -> {
                    toolbarViewModel.titleToolbarView.set(it.data!!.topic)

                    initSetDataNew(it.data.detail?:"")
                    if (it.data.is_choose_participated?:0 == 1){
                        viewModel.isShowButton.set(true)
                    }else{
                        viewModel.isShowButton.set(false)
                    }
                }
                Status.ERROR -> {
                    if (it.message.equals("401")){
                        viewModel.mRefreshTokenCall.call()
                    }else{
                        mDialogPresenter.dialogMessage(
                                resources.getString(R.string.message_alert_dialog),
                                it.message
                        ) {}
                    }
                }
                Status.LOADING -> {
                }
            }
        })

        viewModel.mResponseRefreshToken.observe(this, Observer {
            binding.loadResource = it
            when (it.status) {
                Status.SUCCESS -> {
                    mPreferences.saveToken(it.data!!.accessToken?:"")
                    viewModel.mNewsCall.value = viewModel.mNewsCall.value
                }
                Status.ERROR -> {
                    if (it.message.equals("401")){
                        val intentApp = Intent(this, PreLoginActivity::class.java)
                        startActivity(intentApp)
                        finishAffinity()
                        startIntentAnimation(false)
                    }else{
                        mDialogPresenter.dialogMessage(
                                resources.getString(R.string.message_alert_dialog),
                                it.message
                        ) {}
                    }
                }
                Status.LOADING -> {
                }
            }
        })
    }

    @SuppressLint("SetJavaScriptEnabled")
    fun initSetDataNew(content: String) {
        binding.webView.loadData(content, "text/html", null)
//        binding.webView.isHorizontalScrollBarEnabled = false
//        binding.webView.settings.javaScriptEnabled = true
//        binding.webView.setBackgroundColor(Color.TRANSPARENT)
//        binding.webView.webViewClient = object : WebViewClient() {
//            override fun onPageFinished(view: WebView, url: String) {
//                view.loadUrl(
//                        "javascript:document.body.style.setProperty(\"color\", \"white\");"
//                )
//            }
//
//            override fun shouldOverrideUrlLoading(view: WebView?, request: String?): Boolean {
//                return true
//            }
//        }

        binding.webView.settings.javaScriptEnabled = true
        binding.webView.setBackgroundColor(Color.TRANSPARENT)
        binding.webView.requestFocusFromTouch()
        binding.webView.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
                view.loadUrl(url)
                return true
            }

            //Show loader on url load
            override fun onLoadResource(view: WebView, url: String) {}

            override fun onPageFinished(view: WebView, url: String) {
            }
        }
        binding.webView.settings.setSupportZoom(true)
        binding.webView.settings.builtInZoomControls = true
        binding.webView.settings.loadWithOverviewMode = true
        binding.webView.settings.useWideViewPort = true

    }

    private fun onSubscriptOnClick() {
        viewModel.mOnClickListener.observe(this, Observer {
            val intentApp = Intent(this, ActivityFromActivity::class.java)
            intentApp.putExtra("newsId",viewModel.mNewsCall.value)
            startActivity(intentApp)
            startIntentAnimation(true)
        })

        viewModel.onClickItemList.observe(this, Observer {

        })

        toolbarViewModel.onClickToolbar.observe(this, Observer {
            when (it) {
                "intentBack" -> {
                    this.onBackPressed()
                }
                else -> {
                    print("no event")
                }
            }
        })
    }

    override fun onBackPressed() {
        finish()
        startIntentAnimation( false)
    }
}
