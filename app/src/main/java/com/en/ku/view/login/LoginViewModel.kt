package com.en.ku.view.login

import androidx.databinding.ObservableField
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.en.ku.domain.GeneralUseCase
import com.en.ku.utils.SingleLiveData
import com.en.ku.utils.TextHelper
import com.en.ku.utils.watcher.TextWatcherAdapter
import com.en.ku.vo.model.body.BodyLogin

class LoginViewModel (private val generalUseCase: GeneralUseCase) : ViewModel() {

    val etUserName = ObservableField("")

    val etPassWord = ObservableField("")

    val isStatusButtonClick = ObservableField(false)

    var mLoginCall = SingleLiveData<BodyLogin>()

    var mOnClickListener = SingleLiveData<String>()

    val onUserNameTextChanged = TextWatcherAdapter { s ->
        etUserName.set(s)
        checkEventButtonClick()
    }

    val onPasswordTextChanged = TextWatcherAdapter { s ->
        etPassWord.set(s)
        checkEventButtonClick()
    }

    fun onClickLogin() {
        mOnClickListener.value = "intentLogin"
    }

    fun onClickCancel() {
        mOnClickListener.value = "intentCancel"
    }

    fun onClickForgotPassword() {
        mOnClickListener.value = "intentForgotPassword"
    }

    fun onClickRegister() {
        mOnClickListener.value = "intentRegister"
    }

    val mResponseLogin = Transformations.switchMap(mLoginCall) {
        generalUseCase.onLogin(etUserName.get()!!,etPassWord.get()!!)
    }

    fun checkEventButtonClick() {
        isStatusButtonClick.set(TextHelper.isNotEmptyStrings(etUserName.get()) && TextHelper.isNotEmptyStrings(etPassWord.get())
        )
    }
}
