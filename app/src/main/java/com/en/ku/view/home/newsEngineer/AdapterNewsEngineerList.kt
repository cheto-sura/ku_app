package com.en.ku.view.home.newsEngineer

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.en.ku.R
import com.en.ku.databinding.ItemNewsEngineerBinding
import com.en.ku.utils.SingleLiveData
import com.en.ku.vo.model.response.ModelNews
import com.en.ku.vo.model.response.ResponseNews
import java.util.*

class AdapterNewsEngineerList(
        private var mContext: Context,
        private var mListMenu: ArrayList<ResponseNews.Item>,
        private var mOnClickList: SingleLiveData<ResponseNews.Item>
) : RecyclerView.Adapter<AdapterNewsEngineerList.ViewHolder>() {

    override fun getItemCount(): Int {
        return mListMenu.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ViewHolder {
        val binding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context), R.layout.item_news_engineer, parent, false
        ) as ItemNewsEngineerBinding

        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        bindText(holder, position)

        holder.binding.root.setOnClickListener {
            mOnClickList.value = mListMenu[position]
        }
    }

    private fun bindText(holder: ViewHolder, position: Int) {
        Glide.with(mContext).load(mListMenu[position].thumbnail).into(holder.binding.ivPhoto)
        holder.binding.tvtitle.text = mListMenu[position].topic
        holder.binding.tvDate.text = mListMenu[position].publish_date?:""//"7 ตุลาคม 2563"
    }

    class ViewHolder(internal var binding: ItemNewsEngineerBinding) :
        RecyclerView.ViewHolder(binding.root)
}
