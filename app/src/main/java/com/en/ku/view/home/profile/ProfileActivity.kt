package com.en.ku.view.home.profile

import android.content.Intent
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.en.ku.R
import com.en.ku.databinding.ActivityProfileBinding
import com.en.ku.view.base.BaseActivity
import com.en.ku.view.login.PreLoginActivity
import com.en.ku.vo.enumClass.Status
import org.koin.androidx.viewmodel.ext.android.viewModel
import qiu.niorgai.StatusBarCompat

class ProfileActivity : BaseActivity() {

    private val viewModel: ProfileViewModel by viewModel()

    private lateinit var binding: ActivityProfileBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        initView()
        initViewModel()
    }

    private fun initView() {
        StatusBarCompat.translucentStatusBar(this, true)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_profile)
        toolbarViewModel.titleToolbarView.set(resources.getString(R.string.menu_student))
    }

    private fun initViewModel() {
        binding.dataViewModel = viewModel
        binding.toolbarViewModel = toolbarViewModel

        onSubscriptViewModel()
        onSubscriptOnClick()
    }

    override fun onResume() {
        super.onResume()
        viewModel.mUserCall.call()
    }

    private fun onSubscriptViewModel() {
        viewModel.mResponseUser.observe(this, Observer {
            binding.loadResource = it
            when (it.status) {
                Status.SUCCESS -> {
                    viewModel.mName.set("${it.data!!.firstname_th} ${it.data.lastname_th}")
                    viewModel.mStudentId.set(it.data.username)
                    viewModel.mEmail.set(it.data.email)
                    viewModel.mPhone.set(it.data.telephone.toString())
                }
                Status.ERROR -> {
                    if (it.message.equals("401")){
                        viewModel.mRefreshTokenCall.call()
                    }else{
                        mDialogPresenter.dialogMessage(
                            resources.getString(R.string.message_alert_dialog),
                            it.message
                        ) {}
                    }
                }
                Status.LOADING -> {
                }
            }
        })

        viewModel.mResponseRefreshToken.observe(this, Observer {
            binding.loadResource = it
            when (it.status) {
                Status.SUCCESS -> {
                    mPreferences.saveToken(it.data!!.accessToken?:"")
                    viewModel.mUserCall.call()
                }
                Status.ERROR -> {
                    if (it.message.equals("401")){
                        val intentApp = Intent(this, PreLoginActivity::class.java)
                        startActivity(intentApp)
                        finishAffinity()
                        startIntentAnimation(false)
                    }else{
                        mDialogPresenter.dialogMessage(
                            resources.getString(R.string.message_alert_dialog),
                            it.message
                        ) {}
                    }
                }
                Status.LOADING -> {
                }
            }
        })
    }

    private fun onSubscriptOnClick() {
        toolbarViewModel.onClickToolbar.observe(this, Observer {
            when (it) {
                "intentBack" -> {
                    this.onBackPressed()
                }
                else -> {
                    print("no event")
                }
            }
        })
    }

    override fun onBackPressed() {
        finish()
        startIntentAnimation( false)
    }

}
