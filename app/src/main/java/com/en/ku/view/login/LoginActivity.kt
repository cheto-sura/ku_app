package com.en.ku.view.login

import android.content.Intent
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.en.ku.R
import com.en.ku.databinding.ActivityLoginBinding
import com.en.ku.view.base.BaseActivity
import com.en.ku.view.home.HomeActivity
import com.en.ku.view.register.RegisterActivity
import com.en.ku.vo.enumClass.Status
import com.google.firebase.FirebaseApp
import com.google.firebase.iid.FirebaseInstanceId
import org.koin.androidx.viewmodel.ext.android.viewModel
import qiu.niorgai.StatusBarCompat

class LoginActivity : BaseActivity() {

    private val viewModel: LoginViewModel by viewModel()

    private lateinit var binding: ActivityLoginBinding

    var mUserType = "student"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initView()
        initViewModel()
    }

    private fun initView() {
        StatusBarCompat.translucentStatusBar(this, true)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login)
    }

    private fun initViewModel() {
        binding.dataViewModel = viewModel
        getIntentData()
        callNewToken()
        onSubscriptViewModel()
        onSubscriptOnClick()
    }

    private fun getIntentData(){
        mUserType = intent.getStringExtra("userType")?:"student"
    }

    private fun onSubscriptViewModel() {
        viewModel.mResponseLogin.observe(this, Observer {
            binding.loadResource = it
            when (it.status) {
                Status.SUCCESS -> {
                    mPreferences.saveToken(it.data!!.accessToken)
                    mPreferences.saveRefreshToken(it.data.refreshToken)
                    onStartAppIntent("intentMain")
                }
                Status.ERROR -> mDialogPresenter.dialogMessage(
                    resources.getString(R.string.message_alert_dialog),
                    it.message
                ) {}
                Status.LOADING -> {}
            }
        })
    }

    private fun onSubscriptOnClick() {
        viewModel.mOnClickListener.observe(this, Observer {
            onStartAppIntent(it)
        })
    }

    private fun onStartAppIntent(actionPage: String) {
        val intentApp: Intent
        when (actionPage) {
            "intentMain" -> {
                intentApp = Intent(this, HomeActivity::class.java)
                intentApp.putExtra("userType",mUserType)
                startActivity(intentApp)
                startIntentAnimation(true)
                finishAffinity()
            }
            "intentLogin" -> {
                viewModel.mLoginCall.call()
//                intentApp = Intent(this, HomeActivity::class.java)
//                intentApp.putExtra("userType",mUserType)
//                startActivity(intentApp)
//                startIntentAnimation(true)
//                finishAffinity()
            }
            "intentCancel" -> {

            }
            "intentForgotPassword" -> {

            }
            "intentRegister" -> {
                intentApp = Intent(this, RegisterActivity::class.java)
                startActivity(intentApp)
                startIntentAnimation(true)
            }
        }
        startIntentAnimation( true)
    }

    private fun callNewToken() {
        FirebaseApp.initializeApp(this)
        FirebaseInstanceId.getInstance().instanceId.addOnSuccessListener(this) { instanceIdResult ->
            mPreferences.savePushToken(instanceIdResult.token)
        }
    }
}
