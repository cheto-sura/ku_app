package com.en.ku.view.login

import androidx.lifecycle.ViewModel
import com.en.ku.domain.GeneralUseCase
import com.en.ku.utils.SingleLiveData

class PreLoginViewModel (private val generalUseCase: GeneralUseCase) : ViewModel() {

    var mOnClickListener = SingleLiveData<String>()

    fun onClickLogin() {
        mOnClickListener.value = "Login"
    }

    fun onClickParents() {
        mOnClickListener.value = "Parents"
    }

    fun onClickGeneral() {
        mOnClickListener.value = "General"
    }

}
