package com.en.ku.view.home.notification

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.en.ku.R
import com.en.ku.databinding.ItemNotificationBinding
import com.en.ku.utils.SingleLiveData
import com.en.ku.vo.model.response.ModelNotification
import com.en.ku.vo.model.response.ResponseNews
import java.util.*

class AdapterNotificationList(
        private var mContext: Context,
        private var mListNotification: ArrayList<ResponseNews.Item>,
        private var mOnClickList: SingleLiveData<ResponseNews.Item>
) : RecyclerView.Adapter<AdapterNotificationList.ViewHolder>() {

    override fun getItemCount(): Int {
        return mListNotification.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ViewHolder {
        val binding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context), R.layout.item_notification, parent, false
        ) as ItemNotificationBinding

        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        bindText(holder, position)

        holder.binding.root.setOnClickListener {
            mOnClickList.value = mListNotification[position]
        }
    }

    private fun bindText(holder: ViewHolder, position: Int) {
        Glide.with(mContext).load(mListNotification[position].thumbnail).into(holder.binding.ivPhoto)
        holder.binding.tvtitle.text = mListNotification[position].topic
    }

    class ViewHolder(internal var binding: ItemNotificationBinding) :
        RecyclerView.ViewHolder(binding.root)
}
