package com.en.ku.view.register

import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.en.ku.domain.GeneralUseCase
import com.en.ku.utils.SingleLiveData
import com.en.ku.utils.watcher.TextWatcherAdapter
import com.en.ku.vo.Resource
import com.en.ku.vo.model.body.BodyRegister
import com.en.ku.vo.model.response.ResponseMajor
import com.en.ku.vo.model.response.ResponseRegister
import com.en.ku.vo.model.response.ResponseTitleName

class RegisterViewModel (generalUseCase: GeneralUseCase) : ViewModel() {

    var mOnClickListener = SingleLiveData<String>()

    val etUserName = ObservableField("")

    val etEmail = ObservableField("")

    val etFirstName = ObservableField("")

    val etLastName = ObservableField("")

    val etPassword = ObservableField("")

    val mGender = ObservableField("")

    val mRoleId = ObservableField("")

    val mTitleId = ObservableField("")

    val mMajorId = ObservableField("")

    val etTelephone = ObservableField("")

    val etTitleName = ObservableField("")

    val etMajor = ObservableField("")

    val isStatusButtonClick = ObservableField(false)

    val isStudent = ObservableField(true)

    val onUserNameTextChanged = TextWatcherAdapter { s ->
        etUserName.set(s)
        checkEventButtonClick()
    }

    val onEmailTextChanged = TextWatcherAdapter { s ->
        etEmail.set(s)
        checkEventButtonClick()
    }

    val onFirstNameTextChanged = TextWatcherAdapter { s ->
        etFirstName.set(s)
        checkEventButtonClick()
    }

    val onLastNameTextChanged = TextWatcherAdapter { s ->
        etLastName.set(s)
        checkEventButtonClick()
    }

    val onPasswordTextChanged = TextWatcherAdapter { s ->
        etPassword.set(s)
        checkEventButtonClick()
    }

    val onTelephoneTextChanged = TextWatcherAdapter { s ->
        etTelephone.set(s)
        checkEventButtonClick()
    }

    val onTitleNameTextChanged = TextWatcherAdapter { s ->
        etTitleName.set(s)
        checkEventButtonClick()
    }

    val onMajorTextChanged = TextWatcherAdapter { s ->
        etMajor.set(s)
        checkEventButtonClick()
    }

    var mTitleNameCall = SingleLiveData<Void>()
    val mResponseTitleName: LiveData<Resource<ResponseTitleName>> = Transformations
            .switchMap(mTitleNameCall) {
                generalUseCase.onTitleName()
            }

    var mMajorCall = SingleLiveData<Void>()
    val mResponseMajor: LiveData<Resource<ResponseMajor>> = Transformations
        .switchMap(mMajorCall) {
            generalUseCase.onMajor()
        }

    var mRegisterCall = SingleLiveData<Void>()
    val mResponseRegister: LiveData<Resource<ResponseRegister>> = Transformations
        .switchMap(mRegisterCall) {
            generalUseCase.onRegister(postDataRegister())
        }

    private fun postDataRegister(): BodyRegister {
        return if (isStudent.get()!!){
            BodyRegister(
                etUserName.get()!!,
                etPassword.get()!!,
                etFirstName.get()!!,
                etLastName.get()!!,
                mGender.get()!!,
                etTelephone.get()!!,
                etEmail.get()!!,
                mTitleId.get()!!,
                mMajorId.get()!!,
                "1",
                "1"
            )
        }else{
            BodyRegister(
                etUserName.get()!!,
                etPassword.get()!!,
                etFirstName.get()!!,
                etLastName.get()!!,
                mGender.get()!!,
                etTelephone.get()!!,
                etEmail.get()!!,
                mTitleId.get()!!,
                null,
                "1",
                "1"
            )
        }
    }

    fun onClickEventTitleName() {
        mOnClickListener.value = "titleName"
    }

    fun onClickEventMajor() {
        mOnClickListener.value = "major"
    }

    fun onClickEventRegister() {
        mOnClickListener.value = "register"
    }

    fun checkEventButtonClick() {
        if (isCheckFieldEntry()) {
                isStatusButtonClick.set(true)
        } else {
                isStatusButtonClick.set(false)
        }
    }

    private fun isCheckFieldEntry(): Boolean {
        return if (isStudent.get()!!){
            etUserName.get()!!.isNotEmpty() &&
                    etPassword.get()!!.isNotEmpty() &&
                    etFirstName.get()!!.isNotEmpty() &&
                    etLastName.get()!!.isNotEmpty() &&
                    mGender.get()!!.isNotEmpty() &&
                    etTelephone.get()!!.isNotEmpty() &&
                    etEmail.get()!!.isNotEmpty() &&
                    mTitleId.get()!!.isNotEmpty()&&
                    mMajorId.get()!!.isNotEmpty()&&
                    mRoleId.get()!!.isNotEmpty()
        }else{
            etUserName.get()!!.isNotEmpty() &&
                    etPassword.get()!!.isNotEmpty() &&
                    etFirstName.get()!!.isNotEmpty() &&
                    etLastName.get()!!.isNotEmpty() &&
                    mGender.get()!!.isNotEmpty() &&
                    etTelephone.get()!!.isNotEmpty() &&
                    etEmail.get()!!.isNotEmpty() &&
                    mTitleId.get()!!.isNotEmpty()&&
                    mRoleId.get()!!.isNotEmpty()
        }
    }
}
