package com.en.ku.view.register

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.en.ku.R
import com.en.ku.databinding.ActivityRegisterBinding
import com.en.ku.view.base.BaseActivity
import com.en.ku.vo.enumClass.Status
import com.en.ku.vo.model.response.ResponseMajor
import com.en.ku.vo.model.response.ResponseTitleName
import org.koin.androidx.viewmodel.ext.android.viewModel
import qiu.niorgai.StatusBarCompat

class RegisterActivity : BaseActivity() {

    private val viewModel: RegisterViewModel by viewModel()

    private lateinit var binding: ActivityRegisterBinding

    val mTitleName :ArrayList<ResponseTitleName.Item> = ArrayList()

    val mMajorList :ArrayList<ResponseMajor.Item> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        initView()
        initViewModel()
    }

    private fun initView() {
        StatusBarCompat.translucentStatusBar(this, true)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_register)
        toolbarViewModel.titleToolbarView.set(resources.getString(R.string.message_register))
    }

    private fun initViewModel() {
        binding.dataViewModel = viewModel
        binding.toolbarViewModel = toolbarViewModel

        onSubScriptViewModel()
        onClickListener()
        initRadioButton()
    }

    private fun initRadioButton(){
        binding.radioGender.setOnCheckedChangeListener { group, checkedId ->
            val id: Int = binding.radioGender.checkedRadioButtonId
            if (id!=-1){ // If any radio button checked from radio group
                // Get the instance of radio button using id
                when(id){
                    R.id.radioMale -> {
                        viewModel.mGender.set("2")
                        viewModel.checkEventButtonClick()
                    }
                    R.id.radioFemale -> {
                        viewModel.mGender.set("1")
                        viewModel.checkEventButtonClick()
                    }
                }
            }else{
                // If no radio button checked in this radio group
                viewModel.mGender.set("")
                viewModel.checkEventButtonClick()
            }
        }

        binding.radioUserType.setOnCheckedChangeListener { group, checkedId ->
            val id: Int = binding.radioUserType.checkedRadioButtonId
            if (id!=-1){ // If any radio button checked from radio group
                // Get the instance of radio button using id
                when(id){
                    R.id.radioStudent -> {
                        viewModel.mRoleId.set("3")
                        viewModel.checkEventButtonClick()
                    }
                    R.id.radioParents -> {
                        viewModel.mRoleId.set("2")
                        viewModel.checkEventButtonClick()
                    }
                }
            }else{
                // If no radio button checked in this radio group
                viewModel.mRoleId.set("0")
                viewModel.checkEventButtonClick()
            }
        }
    }

    private fun onSubScriptViewModel() {
        viewModel.mResponseTitleName.observe(this, Observer {
            binding.loadResource = it
            when (it.status) {
                Status.SUCCESS -> {
                    val mList :ArrayList<String> = ArrayList()
                    mTitleName.clear()
                    if (it.data!!.items.isNotEmpty()){
                        mTitleName.addAll(it.data.items)
                        for (i in it.data.items.indices){
                            mList.add(it.data.items[i].name_th)
                        }
                        mDialogPresenter.dialogBottom(mList,0,getString(R.string.message_user_type)){
                            viewModel.etTitleName.set(it)
                            val p = mList.indexOf(it)
                            viewModel.mTitleId.set(mTitleName[p].id)
                        }
                    }

                }
                Status.ERROR -> mDialogPresenter.dialogMessage(
                        resources.getString(R.string.message_alert_dialog),
                        it.message
                ) {}
                Status.LOADING -> {}
            }
        })

        viewModel.mResponseMajor.observe(this, Observer {
            binding.loadResource = it
            when (it.status) {
                Status.SUCCESS -> {
                    val mList :ArrayList<String> = ArrayList()
                    mMajorList.clear()
                    if (it.data!!.items.isNotEmpty()){
                        mMajorList.addAll(it.data.items)
                        for (i in it.data.items.indices){
                            mList.add(it.data.items[i].name_th)
                        }
                        mDialogPresenter.dialogBottom(mList,0,getString(R.string.message_major)){
                            viewModel.etMajor.set(it)
                            val p = mList.indexOf(it)
                            viewModel.mMajorId.set(mMajorList[p].id)
                        }
                    }

                }
                Status.ERROR -> mDialogPresenter.dialogMessage(
                    resources.getString(R.string.message_alert_dialog),
                    it.message
                ) {}
                Status.LOADING -> {}
            }
        })

        viewModel.mResponseRegister.observe(this, Observer {
            binding.loadResource = it
            when (it.status) {
                Status.SUCCESS -> {
                    mDialogPresenter.dialogMessage(
                        resources.getString(R.string.message_alert_dialog),
                        getString(R.string.message_register_success)
                    ) {
                        onStartAppIntent()
                    }
                }
                Status.ERROR -> mDialogPresenter.dialogMessage(
                    resources.getString(R.string.message_alert_dialog),
                    it.message
                ) {}
                Status.LOADING -> {}
            }
        })
    }

    private fun onClickListener() {
        viewModel.mOnClickListener.observe(this, Observer {
            when(it){
                "titleName" -> {
                    viewModel.mTitleNameCall.call()
                }
                "major" -> {
                    viewModel.mMajorCall.call()
                }
                "register" -> {
                    viewModel.mRegisterCall.call()
                }
            }
        })

        toolbarViewModel.onClickToolbar.observe(this, Observer {
            when (it) {
                "intentBack" -> {
                    this.onBackPressed()
                }
                else -> {
                    print("no event")
                }
            }
        })
    }

    private fun onStartAppIntent() {
        this.onBackPressed()
    }

    override fun onBackPressed() {
        finish()
        startIntentAnimation( false)
    }
}
