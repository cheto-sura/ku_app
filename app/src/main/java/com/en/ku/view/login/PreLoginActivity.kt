package com.en.ku.view.login

import android.content.Intent
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.en.ku.R
import com.en.ku.databinding.ActivityPreLoginBinding
import com.en.ku.view.base.BaseActivity
import com.en.ku.view.home.newsEngineer.NewsEngineerActivity
import org.koin.androidx.viewmodel.ext.android.viewModel
import qiu.niorgai.StatusBarCompat

class PreLoginActivity : BaseActivity() {

    private val viewModel: PreLoginViewModel by viewModel()

    private lateinit var binding: ActivityPreLoginBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        initView()
        initViewModel()
    }

    private fun initView() {
        StatusBarCompat.translucentStatusBar(this, true)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_pre_login)
    }

    private fun initViewModel() {
        binding.dataViewModel = viewModel

        onSubscriptViewModel()
        onSubscriptOnClick()
    }

    private fun onSubscriptViewModel() {

    }

    private fun onSubscriptOnClick() {
        viewModel.mOnClickListener.observe(this, Observer {
            onStartAppIntent(it)
        })
    }

    private fun onStartAppIntent(actionPage: String) {
        val intentApp: Intent
        when (actionPage) {
            "Login" -> {
                mPreferences.saveUserType("student")
                intentApp = Intent(this, LoginActivity::class.java)
                intentApp.putExtra("userType","student")
                startActivity(intentApp)
            }
            "Parents" -> {
                mPreferences.saveUserType("parent")
                intentApp = Intent(this, LoginActivity::class.java)
                intentApp.putExtra("userType","parent")
                startActivity(intentApp)
            }
            "General" -> {
                intentApp = Intent(this, NewsEngineerActivity::class.java)
                startActivity(intentApp)
                finishAffinity()
            }
        }
        startIntentAnimation( true)
    }
}
