package com.en.ku.view.home.profile

import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.en.ku.domain.GeneralUseCase
import com.en.ku.utils.SingleLiveData
import com.en.ku.vo.Resource
import com.en.ku.vo.model.response.ResponseMajor
import com.en.ku.vo.model.response.ResponseRefreshToken
import com.en.ku.vo.model.response.ResponseUser

class ProfileViewModel (private val generalUseCase: GeneralUseCase) : ViewModel() {

    val mStudentId = ObservableField("-")

    val mName = ObservableField("-")

    val mEmail = ObservableField("-")

    val mPhone = ObservableField("-")

    val mNisitType = ObservableField("-")

    val mClass = ObservableField("-")

    val mDegree = ObservableField("-")

    val mFaculty = ObservableField("-")

    val mMajors = ObservableField("-")

    val mCampus = ObservableField("-")

    val mCourseType = ObservableField("-")

    val mStudentStatus = ObservableField("-")

    val mAdvisor = ObservableField("-")

    var mUserCall = SingleLiveData<Void>()
    val mResponseUser: LiveData<Resource<ResponseUser>> = Transformations
        .switchMap(mUserCall) {
            generalUseCase.onUser()
        }

    var mRefreshTokenCall = SingleLiveData<Void>()
    val mResponseRefreshToken: LiveData<Resource<ResponseRefreshToken>> = Transformations
        .switchMap(mRefreshTokenCall) {
            generalUseCase.onRefreshToken()
        }
}
