package com.en.ku.view.main

import androidx.databinding.ObservableField
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.en.ku.domain.GeneralUseCase
import com.en.ku.utils.SingleLiveData

class MainViewModel constructor(generalUseCase: GeneralUseCase) : ViewModel() {
    val mCurrentPage = ObservableField(1)

    val isLoadDuplicate = ObservableField(true)

    val onClickItemOrderList = SingleLiveData<String>()

    val mOrderBookingCall = SingleLiveData<Void>()
    val mResponseOrderBooking = Transformations.switchMap(mOrderBookingCall) {
        generalUseCase.getOrderList(mCurrentPage.get()!!)
    }
}
