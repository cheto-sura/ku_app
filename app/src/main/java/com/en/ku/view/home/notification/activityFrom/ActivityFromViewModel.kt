package com.en.ku.view.home.notification.activityFrom

import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.en.ku.domain.GeneralUseCase
import com.en.ku.utils.SingleLiveData
import com.en.ku.utils.watcher.TextWatcherAdapter
import com.en.ku.vo.Resource
import com.en.ku.vo.model.response.ResponseRefreshToken
import com.en.ku.vo.model.response.ResponseUser
import com.en.ku.vo.model.response.ResponseUserActivity

class ActivityFromViewModel (private val generalUseCase: GeneralUseCase) : ViewModel() {

    var mOnClickListener = SingleLiveData<String>()

    val mUserId = ObservableField(0)

    val mNewsId = ObservableField(0)

    val etName = ObservableField("")

    val etLevel = ObservableField("")

    val etNote1 = ObservableField("")

    val etNote2 = ObservableField("")

    val onNameTextChanged = TextWatcherAdapter { s ->
        etName.set(s)
    }

    val onLevelTextChanged = TextWatcherAdapter { s ->
        etLevel.set(s)
    }

    val onNote1TextChanged = TextWatcherAdapter { s ->
        etNote1.set(s)
    }

    val onNote2TextChanged = TextWatcherAdapter { s ->
        etNote2.set(s)
    }

    var mUserCall = SingleLiveData<Void>()
    val mResponseUser: LiveData<Resource<ResponseUser>> = Transformations
        .switchMap(mUserCall) {
            generalUseCase.onUser()
        }

//    var mUserActivityCall = SingleLiveData<Void>()
//    val mResponseUserActivity: LiveData<Resource<ResponseUserActivity>> = Transformations
//        .switchMap(mUserActivityCall) {
//            generalUseCase.onUserActivity(
//                mNewsId.get(),
//            )
//        }

    var mRefreshTokenCall = SingleLiveData<Void>()
    val mResponseRefreshToken: LiveData<Resource<ResponseRefreshToken>> = Transformations
        .switchMap(mRefreshTokenCall) {
            generalUseCase.onRefreshToken()
        }

    fun onClickConfirm() {
        mOnClickListener.value = "confirm"
    }

}
