package com.en.ku.view.home.notification

import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.en.ku.domain.GeneralUseCase
import com.en.ku.utils.SingleLiveData
import com.en.ku.vo.Resource
import com.en.ku.vo.model.response.ResponseNewsDetail
import com.en.ku.vo.model.response.ResponseRefreshToken

class NotificationDetailViewModel (private val generalUseCase: GeneralUseCase) : ViewModel() {

    var mOnClickListener = SingleLiveData<String>()

    val mDetail = ObservableField("")

    var mNewsCall = SingleLiveData<Int>()
    val mResponseNews: LiveData<Resource<ResponseNewsDetail>> = Transformations
            .switchMap(mNewsCall) {
                generalUseCase.onNewsDetail(it)
            }

    var mRefreshTokenCall = SingleLiveData<Void>()
    val mResponseRefreshToken: LiveData<Resource<ResponseRefreshToken>> = Transformations
            .switchMap(mRefreshTokenCall) {
                generalUseCase.onRefreshToken()
            }

    fun onClickConfirm() {
        mOnClickListener.value = "confirm"
    }

}
