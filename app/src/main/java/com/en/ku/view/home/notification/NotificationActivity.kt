package com.en.ku.view.home.notification

import android.content.Intent
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.en.ku.R
import com.en.ku.databinding.ActivityNotificationBinding
import com.en.ku.view.base.BaseActivity
import com.en.ku.view.home.news.NewsDetailActivity
import com.en.ku.view.login.PreLoginActivity
import com.en.ku.vo.enumClass.Status
import com.en.ku.vo.model.response.ModelNotification
import com.en.ku.vo.model.response.ResponseNews
import org.koin.androidx.viewmodel.ext.android.viewModel
import qiu.niorgai.StatusBarCompat

class NotificationActivity : BaseActivity() {

    private val viewModel: NotificationViewModel by viewModel()

    private lateinit var binding: ActivityNotificationBinding

    private var mListDataNotificationList = ArrayList<ResponseNews.Item>()

    private val mCustomAdapterNotificationList by lazy {
        AdapterNotificationList(this, mListDataNotificationList, viewModel.onClickItemList)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        initView()
        initViewModel()
    }

    private fun initView() {
        StatusBarCompat.translucentStatusBar(this, true)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_notification)
        toolbarViewModel.titleToolbarView.set(resources.getString(R.string.menu_notification))
    }

    private fun initViewModel() {
        binding.dataViewModel = viewModel
        binding.toolbarViewModel = toolbarViewModel

        initMenuList()
        onSubscriptViewModel()
        onSubscriptOnClick()
    }

    private fun initMenuList() {
        binding.recyclerViewNews.apply {
            layoutManager = LinearLayoutManager(this@NotificationActivity)
            addOnScrollListener(onScrollListener())
            adapter = mCustomAdapterNotificationList
        }
        mCustomAdapterNotificationList.notifyDataSetChanged()
    }

    override fun onResume() {
        super.onResume()
        viewModel.mNewsCall.call()
    }

    private fun onSubscriptViewModel() {
        viewModel.mResponseNews.observe(this, Observer {
            binding.loadResource = it
            when (it.status) {
                Status.SUCCESS -> {
                    if (viewModel.mPage.get() == 1)
                        mListDataNotificationList.clear()
                    for (i in it.data!!.items.indices){
                        mListDataNotificationList.add(it.data.items[i])
                    }
                    viewModel.isLoadDuplicate.set(it.data.total_page != (viewModel.mPage.get()!!))
                    mCustomAdapterNotificationList.notifyDataSetChanged()
                }
                Status.ERROR -> {
                    if (it.message.equals("401")){
                        viewModel.mRefreshTokenCall.call()
                    }else{
                        mDialogPresenter.dialogMessage(
                                resources.getString(R.string.message_alert_dialog),
                                it.message
                        ) {}
                    }
                }
                Status.LOADING -> {
                }
            }
        })

        viewModel.mResponseRefreshToken.observe(this, Observer {
            binding.loadResource = it
            when (it.status) {
                Status.SUCCESS -> {
                    mPreferences.saveToken(it.data!!.accessToken?:"")
                    viewModel.mNewsCall.call()
                }
                Status.ERROR -> {
                    if (it.message.equals("401")){
                        val intentApp = Intent(this, PreLoginActivity::class.java)
                        startActivity(intentApp)
                        finishAffinity()
                        startIntentAnimation(false)
                    }else{
                        mDialogPresenter.dialogMessage(
                                resources.getString(R.string.message_alert_dialog),
                                it.message
                        ) {}
                    }
                }
                Status.LOADING -> {
                }
            }
        })
    }

    private fun onScrollListener(): RecyclerView.OnScrollListener {
        return object : RecyclerView.OnScrollListener() {
            override fun onScrolled(view: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(view, dx, dy)
                val linearLayoutManager = view.layoutManager as LinearLayoutManager?

                if (linearLayoutManager!!.findLastCompletelyVisibleItemPosition() >= linearLayoutManager.itemCount - 1 && viewModel.isLoadDuplicate.get()!!) {
                    viewModel.mPage.set(viewModel.mPage.get()!!)
                    mCustomAdapterNotificationList.notifyDataSetChanged()
                    viewModel.mNewsCall.call()
                }
            }
        }
    }

    private fun onSubscriptOnClick() {
        viewModel.onClickItemList.observe(this, Observer {
            val intentApp = Intent(this, NewsDetailActivity::class.java)
            intentApp.putExtra("id",it.id)
            startActivity(intentApp)
            startIntentAnimation(true)
        })

        toolbarViewModel.onClickToolbar.observe(this, Observer {
            when (it) {
                "intentBack" -> {
                    this.onBackPressed()
                }
                else -> {
                    print("no event")
                }
            }
        })
    }

    override fun onBackPressed() {
        finish()
        startIntentAnimation( false)
    }
}
