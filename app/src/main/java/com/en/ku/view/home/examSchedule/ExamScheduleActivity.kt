package com.en.ku.view.home.examSchedule

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.View
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.en.ku.R
import com.en.ku.databinding.ActivityExamScheduleBinding
import com.en.ku.view.base.BaseActivity
import com.en.ku.view.login.PreLoginActivity
import com.en.ku.vo.enumClass.Status
import org.koin.androidx.viewmodel.ext.android.viewModel
import qiu.niorgai.StatusBarCompat

class ExamScheduleActivity : BaseActivity() {

    private val viewModel: ExamScheduleViewModel by viewModel()

    private lateinit var binding: ActivityExamScheduleBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        initView()
        initViewModel()
    }

    private fun initView() {
        StatusBarCompat.translucentStatusBar(this, true)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_exam_schedule)
        toolbarViewModel.titleToolbarView.set(resources.getString(R.string.message_exam_schedule))
    }

    private fun initViewModel() {
        binding.dataViewModel = viewModel
        binding.toolbarViewModel = toolbarViewModel

        onSubscriptViewModel()
        onSubscriptOnClick()
    }

    private fun onSubscriptViewModel() {
        viewModel.mExamScheduleCall.call()
        viewModel.mResponseExamSchedule.observe(this, Observer {
            binding.loadResource = it
            when (it.status) {
                Status.SUCCESS -> {
                    initSetDataWebview(it.data!!.detail?:"")

//                    initSetDataWebview("<h2>Absolute URLs</h2>\n" +
//                            "<p><a href=\"https://www.w3.org/\">W3C</a></p>\n" +
//                            "<p><a href=\"https://www.google.com/\">Google</a></p>\n" +
//                            "\n" +
//                            "<h2>Relative URLs</h2>\n" +
//                            "<p><a href=\"html_images.asp\">HTML Images</a></p>\n" +
//                            "<p><a href=\"/css/default.asp\">CSS Tutorial</a></p>")
                }

                Status.ERROR -> {
                    if (it.message.equals("401")){
                        viewModel.mRefreshTokenCall.call()
                    }else{
                        mDialogPresenter.dialogMessage(
                                resources.getString(R.string.message_alert_dialog),
                                it.message
                        ) {}
                    }
                }
                Status.LOADING -> {
                }
            }
        })

        viewModel.mResponseRefreshToken.observe(this, Observer {
            binding.loadResource = it
            when (it.status) {
                Status.SUCCESS -> {
                    mPreferences.saveToken(it.data!!.accessToken?:"")
                    viewModel.mExamScheduleCall.call()
                }
                Status.ERROR -> {
                    if (it.message.equals("401")){
                        val intentApp = Intent(this, PreLoginActivity::class.java)
                        startActivity(intentApp)
                        finishAffinity()
                        startIntentAnimation(false)
                    }else{
                        mDialogPresenter.dialogMessage(
                                resources.getString(R.string.message_alert_dialog),
                                it.message
                        ) {}
                    }
                }
                Status.LOADING -> {
                }
            }
        })
    }

    @SuppressLint("SetJavaScriptEnabled")
    fun initSetDataWebview(content: String) {
        binding.webView.loadData(content, "text/html", null)
//        binding.webView.isHorizontalScrollBarEnabled = false
//        binding.webView.settings.javaScriptEnabled = true
////        binding.webView.setBackgroundColor(Color.TRANSPARENT)
//        binding.webView.webViewClient = object : WebViewClient() {
//            override fun onPageFinished(view: WebView, url: String) {
//                view.loadUrl(
//                        "javascript:document.body.style.setProperty(\"color\", \"white\");"
//                )
//            }
//
//            override fun shouldOverrideUrlLoading(view: WebView?, request: String?): Boolean {
//                return true
//            }
//        }

//        binding.webView.loadUrl(mURL)
//        binding.webView.loadUrl(authorizeURI)
        binding.webView.settings.javaScriptEnabled = true
        binding.webView.setBackgroundColor(Color.TRANSPARENT)
        binding.webView.requestFocusFromTouch()
        binding.webView.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
                view.loadUrl(url)
                return true
            }

            //Show loader on url load
            override fun onLoadResource(view: WebView, url: String) {}

            override fun onPageFinished(view: WebView, url: String) {
            }
        }
        binding.webView.settings.setSupportZoom(true)
        binding.webView.settings.builtInZoomControls = true
        binding.webView.settings.loadWithOverviewMode = true
        binding.webView.settings.useWideViewPort = true
    }

    private fun onSubscriptOnClick() {

        toolbarViewModel.onClickToolbar.observe(this, Observer {
            when (it) {
                "intentBack" -> {
                    this.onBackPressed()
                }
                else -> {
                    print("no event")
                }
            }
        })
    }

    override fun onBackPressed() {
        finish()
        startIntentAnimation( false)
    }
}
