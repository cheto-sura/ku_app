package com.en.ku.view.home.setting

import androidx.lifecycle.ViewModel
import com.en.ku.domain.GeneralUseCase
import com.en.ku.utils.SingleLiveData

class SettingViewModel (private val generalUseCase: GeneralUseCase) : ViewModel() {

    var mOnClickListener = SingleLiveData<String>()

    fun onClickProfile() {
        mOnClickListener.value = "profile"
    }

    fun onClickLanguage() {
        mOnClickListener.value = "language"
    }

    fun onClickLogout() {
        mOnClickListener.value = "logout"
    }

}
