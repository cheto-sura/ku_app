package com.en.ku.view.base

import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel
import com.en.ku.utils.SingleLiveData

class ToolbarViewModel : ViewModel() {

    val onClickToolbar = SingleLiveData<String>()

    val titleToolbarView = ObservableField("")

    fun onClickBack() {
        onClickToolbar.value = "intentBack"
    }
}
