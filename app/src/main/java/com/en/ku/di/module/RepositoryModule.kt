package com.en.ku.di.module

import com.en.ku.data.rest.repository.GeneralRepository
import com.en.ku.domain.GeneralUseCase
import org.koin.dsl.module

val repositoryModule = module {

    single { GeneralRepository(get()) }

    single { GeneralUseCase(get(), get()) }

}