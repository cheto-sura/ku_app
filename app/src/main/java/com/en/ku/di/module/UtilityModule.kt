package com.en.ku.di.module

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentActivity
import com.en.ku.data.local.Preferences
import com.en.ku.utils.CheckPermission
import com.en.ku.utils.TokenExpired
import com.en.ku.utils.Utils
import com.en.ku.utils.dialog.DialogPresenter
import com.en.ku.utils.googleMap.MapUtils
import com.en.ku.utils.imageManagement.ConvertUriToFile
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module

val utilityModule = module {

    single { Preferences(androidApplication()) }

    factory { (activity: FragmentActivity) -> DialogPresenter(activity) }

    single { MapUtils(androidApplication()) }

    single { TokenExpired(androidApplication()) }

    single { Utils(androidApplication(), get()) }

    factory { (activity: AppCompatActivity) ->
        CheckPermission(activity, ConvertUriToFile(androidApplication()))
    }
}


