package com.en.ku.di.module

import com.en.ku.view.base.ToolbarViewModel
import com.en.ku.view.home.HomeViewModel
import com.en.ku.view.home.examSchedule.ExamScheduleViewModel
import com.en.ku.view.home.news.NewsDetailViewModel
import com.en.ku.view.home.news.NewsViewModel
import com.en.ku.view.home.newsEngineer.NewsEngineerViewModel
import com.en.ku.view.home.notification.NotificationDetailViewModel
import com.en.ku.view.home.notification.NotificationViewModel
import com.en.ku.view.home.notification.activityFrom.ActivityFromViewModel
import com.en.ku.view.home.profile.ProfileViewModel
import com.en.ku.view.home.setting.SettingViewModel
import com.en.ku.view.login.LoginViewModel
import com.en.ku.view.login.PreLoginViewModel
import com.en.ku.view.main.MainViewModel
import com.en.ku.view.register.RegisterViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {

    viewModel { ToolbarViewModel() }

    viewModel { PreLoginViewModel(get()) }

    viewModel { LoginViewModel(get()) }

    viewModel { RegisterViewModel(get()) }

    viewModel { MainViewModel(get()) }

    viewModel { HomeViewModel(get()) }

    viewModel { SettingViewModel(get()) }

    viewModel { ProfileViewModel(get()) }

    viewModel { NewsViewModel(get()) }

    viewModel { NewsEngineerViewModel(get()) }

    viewModel { NewsDetailViewModel(get()) }

    viewModel { NotificationViewModel(get()) }

    viewModel { NotificationDetailViewModel(get()) }

    viewModel { ActivityFromViewModel(get()) }

    viewModel { ExamScheduleViewModel(get()) }
}