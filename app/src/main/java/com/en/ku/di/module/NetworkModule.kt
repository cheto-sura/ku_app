package com.en.ku.di.module


import com.en.ku.data.rest.APIService
import com.en.ku.data.rest.OkHttpClientBuilder
import com.en.ku.data.rest.RetrofitBuilder
import org.koin.dsl.module

val networkModule = module {

    single { RetrofitBuilder }

    single<APIService> { get<RetrofitBuilder>().build(
        OkHttpClientBuilder.getUrlServer()) }
}
