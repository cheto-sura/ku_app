package com.en.ku.domain

import com.en.ku.data.local.Preferences
import com.en.ku.data.rest.NetworkBoundResource
import com.en.ku.data.rest.repository.GeneralRepository
import com.en.ku.vo.model.body.BodyRegister
import com.en.ku.vo.model.response.*
import io.reactivex.Observable

class GeneralUseCase(
    private val generalRepository: GeneralRepository,
    private val preferences: Preferences
) {

    fun onLogin(username: String, password: String) =
        object : NetworkBoundResource<ResponseLogin>() {
            override fun saveCallResult(item: String) {}
            override fun createCall(): Observable<ResponseLogin> =
                generalRepository.onLogin(username, password,preferences.getPushToken()?:"")
        }.asLiveData()

    fun onRefreshToken() =
        object : NetworkBoundResource<ResponseRefreshToken>() {
            override fun saveCallResult(item: String) {}
            override fun createCall(): Observable<ResponseRefreshToken> =
                generalRepository.onRefreshToken(preferences.getRefreshToken())
        }.asLiveData()


    fun onRegister(bodyRegister: BodyRegister) =
        object : NetworkBoundResource<ResponseRegister>() {
            override fun saveCallResult(item: String) {}
            override fun createCall(): Observable<ResponseRegister> =
                generalRepository.onRegister(bodyRegister)
        }.asLiveData()

    fun onTitleName() =
            object : NetworkBoundResource<ResponseTitleName>() {
                override fun saveCallResult(item: String) {}
                override fun createCall(): Observable<ResponseTitleName> =
                        generalRepository.onTitleName()
            }.asLiveData()

    fun onMajor() =
        object : NetworkBoundResource<ResponseMajor>() {
            override fun saveCallResult(item: String) {}
            override fun createCall(): Observable<ResponseMajor> =
                generalRepository.onMajor()
        }.asLiveData()

    fun onUser() =
        object : NetworkBoundResource<ResponseUser>() {
            override fun saveCallResult(item: String) {}
            override fun createCall(): Observable<ResponseUser> =
                generalRepository.onUser(preferences.getToken())
        }.asLiveData()

    fun onNews(newsTypeId:Int,page:Int) =
            object : NetworkBoundResource<ResponseNews>() {
                override fun saveCallResult(item: String) {}
                override fun createCall(): Observable<ResponseNews> =
                        generalRepository.onNews(preferences.getToken(),newsTypeId, page)
            }.asLiveData()

    fun onNewsDetail(id:Int) =
            object : NetworkBoundResource<ResponseNewsDetail>() {
                override fun saveCallResult(item: String) {}
                override fun createCall(): Observable<ResponseNewsDetail> =
                        generalRepository.onNewsDetail(preferences.getToken(),id)
            }.asLiveData()

    fun onUserActivity(news_id: Int,is_participated_in: Int,reason_id: Int,reason_other: String,comment: String) =
        object : NetworkBoundResource<ResponseUserActivity>() {
            override fun saveCallResult(item: String) {}
            override fun createCall(): Observable<ResponseUserActivity> =
                generalRepository.onUserActivity(
                    preferences.getToken(),
                    preferences.getUserId()!!.toInt(),
                    news_id,
                    is_participated_in,
                    reason_id,
                    reason_other,
                    comment)
        }.asLiveData()

    fun onExamSchedule() =
            object : NetworkBoundResource<ResponseExamSchedule>() {
                override fun saveCallResult(item: String) {}
                override fun createCall(): Observable<ResponseExamSchedule> =
                        generalRepository.onExamSchedule(preferences.getToken())
            }.asLiveData()








    fun getOrderList(pageCurrent: Int) = object : NetworkBoundResource<ResponseOrderList>() {
        override fun saveCallResult(item: String) {}
        override fun createCall(): Observable<ResponseOrderList> =
            generalRepository.getOrderList("th",pageCurrent, preferences.getToken())
    }.asLiveData()

}