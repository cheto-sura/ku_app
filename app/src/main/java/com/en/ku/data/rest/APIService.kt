package com.en.ku.data.rest

import com.en.ku.vo.model.body.BodyLogin
import com.en.ku.vo.model.response.*
import io.reactivex.Observable
import okhttp3.RequestBody
import retrofit2.http.*

interface APIService {

    @POST("shops/login")
    fun doLogin(@Body paramLogin: BodyLogin): Observable<ResponseLogin>

    @GET("shops/bookings")
    fun getOrderBookings(
        @Header("Accept-Language") language: String? = "en_US",
        @Header("Authorization") accessToken: String,
        @Query("page") page: Int
    ): Observable<ResponseOrderList>

    @Multipart
    @POST("auth/register/nisit")
    fun registerUser(
        @PartMap registerBody: MutableMap<String, @JvmSuppressWildcards RequestBody?>?
    ): Observable<ResponseRegister>


    @GET("user_title")
    fun onTitleName(
    ): Observable<ResponseTitleName>

    @GET("major")
    fun onMajor(): Observable<ResponseMajor>

    @FormUrlEncoded
    @POST("auth/login")
    fun onLogin(
            @Field("username")username: String,
            @Field("password")password: String,
            @Field("mobile_token")pushToken: String
    ): Observable<ResponseLogin>

    @FormUrlEncoded
    @POST("auth/refresh_token")
    fun onRefreshToken(
        @Field("refreshToken")username: String
    ): Observable<ResponseRefreshToken>

    @GET("user/info")
    fun onUser(
        @Header("Authorization") accessToken: String
    ): Observable<ResponseUser>

    @GET("news")
    fun onNews(
            @Header("Authorization") accessToken: String,
            @Query("news_type_id") news_type_id: Int,
            @Query("page") page: Int?
    ): Observable<ResponseNews>

    @GET("news/{id}")
    fun onNewsDetail(
            @Header("Authorization") accessToken: String,
            @Path("id") id: Int
    ): Observable<ResponseNewsDetail>

    @FormUrlEncoded
    @POST("user_activity")
    fun onUserActivity(
            @Header("Authorization") accessToken: String,
            @Field("nisit_id")nisit_id: Int,
            @Field("news_id")news_id: Int,
            @Field("is_participated_in")is_participated_in: Int,
            @Field("reason_id")reason_id: Int?,
            @Field("reason_other")reason_other: String?,
            @Field("comment")comment: String
    ): Observable<ResponseUserActivity>

    @GET("exam/current_exam")
    fun onExamSchedule(
            @Header("Authorization") accessToken: String
    ): Observable<ResponseExamSchedule>
}
