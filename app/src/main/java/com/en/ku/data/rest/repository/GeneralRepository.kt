package com.en.ku.data.rest.repository

import com.en.ku.data.rest.APIService
import com.en.ku.utils.ConvertRequestBody.onConvertObjectToMap
import com.en.ku.vo.model.body.BodyRegister
import com.en.ku.vo.model.response.*
import io.reactivex.Observable

class GeneralRepository
constructor(
    private val apiService: APIService
) {

    fun onLogin(username: String,password: String,pushToken:String): Observable<ResponseLogin> {
        return apiService.onLogin(username,password,pushToken)
    }

    fun onRefreshToken(refreshToken: String): Observable<ResponseRefreshToken> {
        return apiService.onRefreshToken(refreshToken)
    }

    fun onRegister(data: BodyRegister): Observable<ResponseRegister> {
        return apiService.registerUser(onConvertObjectToMap(data))
    }

    fun onTitleName(): Observable<ResponseTitleName> {
        return apiService.onTitleName()
    }

    fun onMajor(): Observable<ResponseMajor> {
        return apiService.onMajor()
    }

    fun onUser(token: String): Observable<ResponseUser> {
        return apiService.onUser(token)
    }

    fun onNews(token: String,newsTypeId:Int,page:Int): Observable<ResponseNews> {
        return apiService.onNews(token,newsTypeId, page)
    }

    fun onNewsDetail(token: String,id:Int): Observable<ResponseNewsDetail> {
        return apiService.onNewsDetail(token,id)
    }

    fun onUserActivity(token: String,nisit_id: Int,news_id: Int,is_participated_in: Int,reason_id: Int,reason_other: String,comment: String): Observable<ResponseUserActivity> {
        return apiService.onUserActivity(
            token,
            nisit_id,
            news_id,
            is_participated_in,
            reason_id,
            reason_other,
            comment
        )
    }

    fun onExamSchedule(token: String): Observable<ResponseExamSchedule> {
        return apiService.onExamSchedule(token)
    }






    fun getOrderList(language: String, pageCurrent: Int, token: String) =
        apiService.getOrderBookings(language, token, pageCurrent)
}
