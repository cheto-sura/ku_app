package com.en.ku.vo.model.response

data class ResponseNews(
    val count_item_page: Int,
    val current_page: Int,
    val items: List<Item>,
    val size_per_page: Int,
    val total_item: Int,
    val total_page: Int
){
    data class Item(
            val allowed_user: Any,
            val created_time: String,
            val created_user: Int,
            val deleted_by: Int,
            val detail: String,
            val expire_date: Any,
            val full_user_created: FullUserCreated,
            val full_user_updated: FullUserUpdated,
            val id: Int,
            val is_deleted: Int,
            val is_needed_feedback: Int,
            val is_notified_user: Int,
            val is_sent_group: Int,
            val is_sent_major: Int,
            val is_sent_personal: Int,
            val is_sent_year: Int,
            val news_type_id: Int,
            val news_type_name: String,
            val notification_number: Int,
            val publish_date: String?,
            val status: Int,
            val thumbnail: String,
            val topic: String,
            val updated_time: String,
            val updated_user: Int
    )

    data class FullUserCreated(
            val firstname: String,
            val lastname: String,
            val title: String
    )

    data class FullUserUpdated(
            val firstname: String,
            val lastname: String,
            val title: String
    )
}






