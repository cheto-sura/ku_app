package com.en.ku.vo.model.response

data class ResponseRegister(
    val email: String,
    val firstname: String,
    val gender: String,
    val lastname: String,
    val password: String,
    val role_id: String,
    val telephone: String,
    val title_id: String,
    val username: String
)
