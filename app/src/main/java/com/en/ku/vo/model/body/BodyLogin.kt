package com.en.ku.vo.model.body

data class BodyLogin(val email: String, val password: String, val pushToken: String, val language: String)
