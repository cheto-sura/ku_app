package com.en.ku.vo.model.response

data class ResponseRefreshToken(
    val accessToken: String,
    val expiresIn: Int
)

