package com.en.ku.vo.model.response

data class ModelMenu(
    val id: Int,
    val name: String,
    val photo: Int
)
