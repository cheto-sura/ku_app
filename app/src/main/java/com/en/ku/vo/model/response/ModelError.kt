package com.en.ku.vo.model.response

//data class ModelError(
//    val errors: Errors
//)
//
//data class Errors(val message: String?)

data class ModelError(
    val error: Int,
    val messages: Messages,
    val status: Int
){
    data class Messages(
        val error: String
    )
}


