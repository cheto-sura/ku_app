package com.en.ku.vo.model.response

data class ModelNotification(
    val title: String,
    val description: String
)
