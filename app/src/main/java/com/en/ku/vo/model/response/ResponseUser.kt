package com.en.ku.vo.model.response
data class ResponseUser(
    val active_time: Any,
    val advisor_id: Any,
    val campus_id: Any,
    val category: Int,
    val deleted_by: Int,
    val email: String,
    val firstname_en: Any,
    val firstname_th: String,
    val gender: Int,
    val id: Int,
    val is_actived: Int,
    val is_deleted: Int,
    val last_login_time: Any,
    val lastname_en: Any,
    val lastname_th: String,
    val major_id: Int,
    val mobile_token: String,
    val nisit_year: Int,
    val role_id: Int,
    val telephone: Int,
    val title: Title,
    val title_id: Int,
    val username: String
){
    data class Title(
        val created_time: String,
        val created_user: Int,
        val deleted_by: Int,
        val id: Int,
        val is_deleted: Int,
        val name_en: String,
        val name_th: String,
        val updated_time: String,
        val updated_user: Int
    )
}


