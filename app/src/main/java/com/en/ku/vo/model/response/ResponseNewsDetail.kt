package com.en.ku.vo.model.response

data class ResponseNewsDetail(
    val allowed_user: Any,
    val created_time: String,
    val created_user: Int,
    val deleted_by: Int,
    val detail: String,
    val expire_date: Any,
    val full_user_created: FullUserCreated,
    val full_user_updated: FullUserUpdated,
    val id: Int,
    val is_choose_participated: Int?,
    val is_deleted: Int,
    val is_needed_feedback: Int,
    val is_notified_user: Int,
    val is_popular: Int,
    val is_sent_group: Int,
    val is_sent_major: Int,
    val is_sent_personal: Int,
    val is_sent_year: Int,
    val name: String,
    val news_type_id: Int,
    val notification_number: Int,
    val publish_date: String,
    val status: Int,
    val thumbnail: String,
    val topic: String,
    val updated_time: String,
    val updated_user: Int,
    val user_activity: UserActivity?
){
    data class FullUserCreated(
            val firstname: String,
            val lastname: String,
            val title: String
    )

    data class FullUserUpdated(
            val firstname: String,
            val lastname: String,
            val title: String
    )

    data class UserActivity(
            val comment: String,
            val created_time: String,
            val id: Int,
            val is_participated_in: Int,
            val news_id: Int,
            val nisit_id: Int,
            val participated_time: String,
            val reason_id: Any,
            val reason_other: Any,
            val send_notification_time: String,
            val updated_time: String
    )
}









