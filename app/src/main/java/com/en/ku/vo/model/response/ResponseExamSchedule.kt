package com.en.ku.vo.model.response
data class ResponseExamSchedule(
    val content_files: List<ContentFile>,
    val created_time: String,
    val created_user: Int,
    val deleted_by: Int,
    val detail: String,
    val exam_type_id: Int,
    val exam_type_name: String,
    val full_user_created: FullUserCreated,
    val full_user_updated: FullUserUpdated,
    val id: Int,
    val is_deleted: Int,
    val publish_date: String,
    val school_year: Int,
    val status: Int,
    val updated_time: String,
    val updated_user: Int
){
    data class ContentFile(
            val created_time: String,
            val exam_id: Int,
            val file_type_name: String,
            val filename: String,
            val id: Int,
            val path: String,
            val size: Int,
            val updated_time: String
    )

    data class FullUserCreated(
            val firstname: String,
            val lastname: String,
            val title: String
    )

    data class FullUserUpdated(
            val firstname: String,
            val lastname: String,
            val title: String
    )
}




