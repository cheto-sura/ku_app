package com.en.ku.vo.enumClass

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}
