package com.en.ku.vo.model.response

data class ResponseLogin(
    val accessToken: String,
    val expiresIn: Int,
    val refreshToken: String
)
