package com.en.ku.vo.model.response

data class ResponseMajor(
    val count_item_page: Int,
    val current_page: Int,
    val items: List<Item>,
    val size_per_page: Int,
    val total_item: Int,
    val total_page: Int
){
    data class Item(
        val code: String,
        val created_time: String,
        val created_user: String,
        val deleted_by: String,
        val full_user_created: FullUserCreated,
        val full_user_updated: FullUserUpdated,
        val id: String,
        val is_deleted: String,
        val name_en: String,
        val name_th: String,
        val updated_time: String,
        val updated_user: String
    )

    data class FullUserCreated(
        val firstname: String,
        val lastname: String,
        val title: String
    )

    data class FullUserUpdated(
        val firstname: String,
        val lastname: String,
        val title: String
    )
}


