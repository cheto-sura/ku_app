package com.en.ku.vo.model.body

data class BodyRegister(
    val username: String,
    val password: String,
    val firstname_th: String,
    val lastname_th: String,
    val gender: String,
    val telephone: String,
    val email: String,
    val title_id: String,
    val major_id: String?,
    val category: String,
    val role_id: String
)
