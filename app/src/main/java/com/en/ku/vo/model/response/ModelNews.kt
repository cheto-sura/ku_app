package com.en.ku.vo.model.response

data class ModelNews(
    val id: Int,
    val title: String,
    val date: String,
    val photo: Int
)
