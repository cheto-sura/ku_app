package com.en.ku.vo.enumClass

enum class Language {
    TH,
    EN
}
